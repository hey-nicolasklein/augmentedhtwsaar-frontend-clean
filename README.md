# Augmented HTWSaar Frontend 2019 

This is the clean version of my 4th semester project
at HTWSaar in south-west Germany.
It doesn't contain any api keys or backend logins.
Therefor its just meant for presentation purposes.

Credits to Christopher Jung und Christian Warken with whom I did this project.
