//
//  Config.swift
//  AugmentedRealityHTWSaar
//
//  Created by CHW on 14.07.19
//  Copyright © 2018 AugmentedReality. All rights reserved.
//

import Foundation

/*
 Klasse zum Einstellen der Server-/Localhost Adresse, sowie Aktivierung der REST-Mock zu Testzwecken
 */
class Config{
    static let url: String = "http://localhost:8080"; //localhost
    
    
    static let usingMock = false
}
